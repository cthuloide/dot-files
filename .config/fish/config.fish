# Exports
set fish_greeting
set TERM "xterm-256color"
set EDITOR "nvim"
set TERMINAL "kitty"
set BROWSER "firefox"
set MANPAGER "nvim +Man!"

# Vi Mode
# function fish_user_key_bindings
#   fish_vi_key_bindings
# end

# Aliases
## Bookmarked Directories
alias h "cd ~/"
alias d "cd ~/Documents/"
alias D "cd ~/Downloads/"
alias m "cd ~/Music/"
alias pp "cd ~/Pictures/"
alias t "cd ~/Templates/"
alias vv "cd ~/Videos/"
alias cf "cd ~/.config/"
alias sc "cd ~/.local/bin/"
alias rr "cd ~/.local/src/"
alias dp="cd ~/Documents/projects/"
alias mt "cd ~/Documents/projects/michaeltognarini.com/"
alias xyz "cd ~/Documents/projects/michaeltognarini.xyz/"
alias dw "cd ~/Documents/projects/delveintotheweird.com/"
alias sl "cd ~/Documents/projects/theseatoflearning.com/"

## Bookmarked Files
alias cfs "nvim ~/.config/fish/config.fish"
alias cfv "nvim ~/.config/nvim/init.vim"
alias cft "nvim ~/.config/kitty/kitty.conf"

## Listing Files
alias exa "exa --color=auto --group-directories-first"
alias l "exa"
alias la "exa -a"
alias ls "exa"
alias ll "exa -al"
alias lt "exa -T --level=3"

## Behavior 
alias cp "cp -iv"
alias mv "mv -iv"
alias rm  "rm -vI"
alias mkd "mkdir -pv"

## Long Commands
alias rmt "rsync -rtvzP /home/michael/Documents/projects/michaeltognarini.com/ root@michaeltognarini.com:/var/www/michaeltognarini.com"
alias rxyz "rsync -rtvzP /home/michael/Documents/projects/michaeltognarini.xyz/ root@michaeltognarini.com:/var/www/michaeltognarini.xyz"
alias rdw "rsync -rtvzP /home/michael/Documents/projects/delveintotheweird.com/ root@michaeltognarini.com:/var/www/delveintotheweird.com"
alias rmt "rsync -rtvzP /home/michael/Documents/projects/theseaoflearning.com/ root@michaeltognarini.com:/var/www/theseatoflearning.com"

## Commonly Used Programs
alias e="$EDITOR"
alias v="$EDITOR"

# Starship Prompt
starship init fish | source

call plug#begin('$HOME/.config/nvim/plugged')
Plug 'agude/vim-eldar'
Plug 'itchyny/lightline.vim'
Plug 'sheerun/vim-polyglot'
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-commentary'
call plug#end()

" Main Configurations
filetype plugin indent on
set encoding=utf-8
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab smarttab autoindent
set incsearch ignorecase smartcase hlsearch
set wildmode=longest,list,full wildmenu
set cursorline colorcolumn=80
set scrolloff=10 sidescrolloff=15
set splitright splitbelow
set number relativenumber
set nowrap
set hidden
set title

" File Type Specific Configurations
autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType css setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType xml setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType markdown setlocal shiftwidth=2 tabstop=2 softtabstop=2 wrap

" Color Configuration
colorscheme eldar
hi CursorLine cterm=NONE ctermfg=NONE ctermbg=237 gui=NONE guifg=NONE guifg=NONE
hi ColorColumn cterm=NONE ctermfg=NONE ctermbg=237 gui=NONE guifg=NONE guifg=NONE

" Key Bindings
let mapleader=','

"" Emmet Key Bindings
let g:user_emmet_leader_key=','
